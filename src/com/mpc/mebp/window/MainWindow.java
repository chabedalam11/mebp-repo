/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpc.mebp.window;

import com.mpc.mebp.util.HelpingManager;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 *
 * @author medisys
 */
public class MainWindow extends javax.swing.JFrame {

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        //set background image
        new HelpingManager().setBackgroundImage2(this);
        setResizable(false);
        initComponents();
        //SET HEADING
        new HelpingManager().setHeading(this);
        
        imgLoading.setVisible(false);
        changePanelBG();
        setMemberName();
        pnlWinner.setVisible(false);
    }
    
    private void changePanelBG(){
        panal1.setOpaque(true);
        panal1.setBackground(new Color(5,5,5,25));
        pnlWinner.setOpaque(true);
        pnlWinner.setBackground(new Color(10,100,10,25));
        pnl1.setOpaque(true);
        pnl1.setBackground(new Color(99,25,25,99));
        pnl2.setOpaque(true);
        pnl2.setBackground(new Color(5,5,5,25));
        pnl3.setOpaque(true);
        pnl3.setBackground(new Color(99,25,25,99));
        pnl4.setOpaque(true);
        pnl4.setBackground(new Color(5,5,5,25));
        pnl5.setOpaque(true);
        pnl5.setBackground(new Color(99,25,25,99));
        pnl6.setOpaque(true);
        pnl6.setBackground(new Color(5,5,5,25));
        pnl7.setOpaque(true);
        pnl7.setBackground(new Color(99,25,25,99));
        pnl8.setOpaque(true);
        pnl8.setBackground(new Color(5,5,5,25));
        pnl9.setOpaque(true);
        pnl9.setBackground(new Color(99,25,25,99));
        pnl10.setOpaque(true);
        pnl10.setBackground(new Color(5,5,5,25));
        pnl11.setOpaque(true);
        pnl11.setBackground(new Color(99,25,25,99));
        pnl12.setOpaque(true);
        pnl12.setBackground(new Color(5,5,5,25));
    }
    
    private void setMemberName(){
        Properties prop = new Properties();
	InputStream input = null;

	try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
            
            lblName1.setText(getMemberName(prop.getProperty("1")));
            if(getMemberStatus(prop.getProperty("1")).equals("1")){
                status1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName2.setText(getMemberName(prop.getProperty("2")));
            if(getMemberStatus(prop.getProperty("2")).equals("1")){
                status2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName3.setText(getMemberName(prop.getProperty("3")));
            if(getMemberStatus(prop.getProperty("3")).equals("1")){
                status3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName4.setText(getMemberName(prop.getProperty("4")));
            if(getMemberStatus(prop.getProperty("4")).equals("1")){
                status4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName5.setText(getMemberName(prop.getProperty("5")));
            if(getMemberStatus(prop.getProperty("5")).equals("1")){
                status5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName6.setText(getMemberName(prop.getProperty("6")));
            if(getMemberStatus(prop.getProperty("6")).equals("1")){
                status6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName7.setText(getMemberName(prop.getProperty("7")));
            if(getMemberStatus(prop.getProperty("7")).equals("1")){
                status7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName8.setText(getMemberName(prop.getProperty("8")));
            if(getMemberStatus(prop.getProperty("8")).equals("1")){
                status8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName9.setText(getMemberName(prop.getProperty("9")));
            if(getMemberStatus(prop.getProperty("9")).equals("1")){
                status9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName10.setText(getMemberName(prop.getProperty("10")));
            if(getMemberStatus(prop.getProperty("10")).equals("1")){
                status10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName11.setText(getMemberName(prop.getProperty("11")));
            if(getMemberStatus(prop.getProperty("11")).equals("1")){
                status11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }
            lblName12.setText(getMemberName(prop.getProperty("12")));
            if(getMemberStatus(prop.getProperty("12")).equals("1")){
                status12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/del.png"))); // NOI18N
            }else{
                status12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/ok.png"))); // NOI18N
            }

	} catch (IOException ex) {
		ex.printStackTrace();
	} finally {
            if (input != null) {
                try {
                   input.close();
                } catch (IOException e) {
                   e.printStackTrace();
                }
            }
	}
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panal1 = new javax.swing.JPanel();
        pnl1 = new javax.swing.JPanel();
        status1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblName1 = new javax.swing.JLabel();
        pnl2 = new javax.swing.JPanel();
        status2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblName2 = new javax.swing.JLabel();
        pnl4 = new javax.swing.JPanel();
        status4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblName4 = new javax.swing.JLabel();
        pnl3 = new javax.swing.JPanel();
        status3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblName3 = new javax.swing.JLabel();
        pnl6 = new javax.swing.JPanel();
        status6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lblName6 = new javax.swing.JLabel();
        pnl5 = new javax.swing.JPanel();
        status5 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblName5 = new javax.swing.JLabel();
        pnl7 = new javax.swing.JPanel();
        status7 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        lblName7 = new javax.swing.JLabel();
        pnl8 = new javax.swing.JPanel();
        status8 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        lblName8 = new javax.swing.JLabel();
        pnl9 = new javax.swing.JPanel();
        status9 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        lblName9 = new javax.swing.JLabel();
        pnl10 = new javax.swing.JPanel();
        status10 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        lblName10 = new javax.swing.JLabel();
        pnl11 = new javax.swing.JPanel();
        status11 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        lblName11 = new javax.swing.JLabel();
        pnl12 = new javax.swing.JPanel();
        status12 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        lblName12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnExit = new javax.swing.JButton();
        imgLoading = new javax.swing.JLabel();
        pnlWinner = new javax.swing.JPanel();
        lblWinner = new javax.swing.JLabel();
        lblWinnerlbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        status1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status1.setForeground(new java.awt.Color(102, 102, 102));
        status1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status1MouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("1");

        lblName1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName1.setForeground(new java.awt.Color(102, 102, 102));
        lblName1.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl1Layout = new javax.swing.GroupLayout(pnl1);
        pnl1.setLayout(pnl1Layout);
        pnl1Layout.setHorizontalGroup(
            pnl1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnl1Layout.setVerticalGroup(
            pnl1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status2.setForeground(new java.awt.Color(102, 102, 102));
        status2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status2MouseClicked(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("2");

        lblName2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName2.setForeground(new java.awt.Color(102, 102, 102));
        lblName2.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl2Layout = new javax.swing.GroupLayout(pnl2);
        pnl2.setLayout(pnl2Layout);
        pnl2Layout.setHorizontalGroup(
            pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName2, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl2Layout.setVerticalGroup(
            pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status4.setForeground(new java.awt.Color(102, 102, 102));
        status4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status4MouseClicked(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("4");

        lblName4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName4.setForeground(new java.awt.Color(102, 102, 102));
        lblName4.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl4Layout = new javax.swing.GroupLayout(pnl4);
        pnl4.setLayout(pnl4Layout);
        pnl4Layout.setHorizontalGroup(
            pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName4, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl4Layout.setVerticalGroup(
            pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status3.setForeground(new java.awt.Color(102, 102, 102));
        status3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status3MouseClicked(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("3");

        lblName3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName3.setForeground(new java.awt.Color(102, 102, 102));
        lblName3.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl3Layout = new javax.swing.GroupLayout(pnl3);
        pnl3.setLayout(pnl3Layout);
        pnl3Layout.setHorizontalGroup(
            pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName3, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl3Layout.setVerticalGroup(
            pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status6.setForeground(new java.awt.Color(102, 102, 102));
        status6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status6MouseClicked(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setText("6");

        lblName6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName6.setForeground(new java.awt.Color(102, 102, 102));
        lblName6.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl6Layout = new javax.swing.GroupLayout(pnl6);
        pnl6.setLayout(pnl6Layout);
        pnl6Layout.setHorizontalGroup(
            pnl6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName6, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl6Layout.setVerticalGroup(
            pnl6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status5.setForeground(new java.awt.Color(102, 102, 102));
        status5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status5MouseClicked(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(102, 102, 102));
        jLabel16.setText("5");

        lblName5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName5.setForeground(new java.awt.Color(102, 102, 102));
        lblName5.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl5Layout = new javax.swing.GroupLayout(pnl5);
        pnl5.setLayout(pnl5Layout);
        pnl5Layout.setHorizontalGroup(
            pnl5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName5, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl5Layout.setVerticalGroup(
            pnl5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status7.setForeground(new java.awt.Color(102, 102, 102));
        status7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status7MouseClicked(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(102, 102, 102));
        jLabel24.setText("7");

        lblName7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName7.setForeground(new java.awt.Color(102, 102, 102));
        lblName7.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl7Layout = new javax.swing.GroupLayout(pnl7);
        pnl7.setLayout(pnl7Layout);
        pnl7Layout.setHorizontalGroup(
            pnl7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName7, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl7Layout.setVerticalGroup(
            pnl7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status8.setForeground(new java.awt.Color(102, 102, 102));
        status8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status8MouseClicked(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(102, 102, 102));
        jLabel27.setText("8");

        lblName8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName8.setForeground(new java.awt.Color(102, 102, 102));
        lblName8.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl8Layout = new javax.swing.GroupLayout(pnl8);
        pnl8.setLayout(pnl8Layout);
        pnl8Layout.setHorizontalGroup(
            pnl8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName8, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl8Layout.setVerticalGroup(
            pnl8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status9.setForeground(new java.awt.Color(102, 102, 102));
        status9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status9MouseClicked(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(102, 102, 102));
        jLabel30.setText("9");

        lblName9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName9.setForeground(new java.awt.Color(102, 102, 102));
        lblName9.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl9Layout = new javax.swing.GroupLayout(pnl9);
        pnl9.setLayout(pnl9Layout);
        pnl9Layout.setHorizontalGroup(
            pnl9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName9, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl9Layout.setVerticalGroup(
            pnl9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status10.setForeground(new java.awt.Color(102, 102, 102));
        status10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status10MouseClicked(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(102, 102, 102));
        jLabel33.setText("10");

        lblName10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName10.setForeground(new java.awt.Color(102, 102, 102));
        lblName10.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl10Layout = new javax.swing.GroupLayout(pnl10);
        pnl10.setLayout(pnl10Layout);
        pnl10Layout.setHorizontalGroup(
            pnl10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName10, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl10Layout.setVerticalGroup(
            pnl10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status11.setForeground(new java.awt.Color(102, 102, 102));
        status11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status11MouseClicked(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(102, 102, 102));
        jLabel36.setText("11");

        lblName11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName11.setForeground(new java.awt.Color(102, 102, 102));
        lblName11.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl11Layout = new javax.swing.GroupLayout(pnl11);
        pnl11.setLayout(pnl11Layout);
        pnl11Layout.setHorizontalGroup(
            pnl11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName11, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl11Layout.setVerticalGroup(
            pnl11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        status12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status12.setForeground(new java.awt.Color(102, 102, 102));
        status12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                status12MouseClicked(evt);
            }
        });

        jLabel39.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(102, 102, 102));
        jLabel39.setText("12");

        lblName12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblName12.setForeground(new java.awt.Color(102, 102, 102));
        lblName12.setText("Abu Salah Musa");

        javax.swing.GroupLayout pnl12Layout = new javax.swing.GroupLayout(pnl12);
        pnl12.setLayout(pnl12Layout);
        pnl12Layout.setHorizontalGroup(
            pnl12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblName12, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(status12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnl12Layout.setVerticalGroup(
            pnl12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(status12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblName12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(121, 18, 21));
        jLabel1.setText("MEMBER LIST");
        jLabel1.setAlignmentY(5.0F);

        javax.swing.GroupLayout panal1Layout = new javax.swing.GroupLayout(panal1);
        panal1.setLayout(panal1Layout);
        panal1Layout.setHorizontalGroup(
            panal1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panal1Layout.createSequentialGroup()
                .addGroup(panal1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panal1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(panal1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnl2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnl1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(panal1Layout.createSequentialGroup()
                        .addGap(194, 194, 194)
                        .addComponent(jLabel1)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        panal1Layout.setVerticalGroup(
            panal1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panal1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnl12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );

        btnExit.setBackground(new java.awt.Color(0, 153, 0));
        btnExit.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        btnExit.setText(" [ START ]");
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }
        });
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        imgLoading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/loading2.gif"))); // NOI18N

        lblWinner.setFont(new java.awt.Font("Sylfaen", 1, 18)); // NOI18N
        lblWinner.setForeground(new java.awt.Color(121, 18, 21));
        lblWinner.setText("name");
        lblWinner.setAlignmentY(5.0F);

        lblWinnerlbl.setFont(new java.awt.Font("Sylfaen", 1, 36)); // NOI18N
        lblWinnerlbl.setForeground(new java.awt.Color(121, 18, 21));
        lblWinnerlbl.setText("Winner is");
        lblWinnerlbl.setAlignmentY(5.0F);

        javax.swing.GroupLayout pnlWinnerLayout = new javax.swing.GroupLayout(pnlWinner);
        pnlWinner.setLayout(pnlWinnerLayout);
        pnlWinnerLayout.setHorizontalGroup(
            pnlWinnerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlWinnerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlWinnerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblWinnerlbl, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblWinner, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        pnlWinnerLayout.setVerticalGroup(
            pnlWinnerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlWinnerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblWinnerlbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblWinner)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(imgLoading, javax.swing.GroupLayout.PREFERRED_SIZE, 341, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnlWinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(276, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(pnlWinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(imgLoading, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(359, 359, 359)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(419, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        pnlWinner.setVisible(false);
        imgLoading.setVisible(true);
        abcCaller();
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseClicked
        //imgLoading.setVisible(true);
        //System.out.println("Hello world");
    }//GEN-LAST:event_btnExitMouseClicked

    private void status1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status1MouseClicked
        changeStaus("1");
        setMemberName();
    }//GEN-LAST:event_status1MouseClicked

    private void status2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status2MouseClicked
        changeStaus("2");
        setMemberName();
    }//GEN-LAST:event_status2MouseClicked

    private void status3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status3MouseClicked
       changeStaus("3");
        setMemberName();
    }//GEN-LAST:event_status3MouseClicked

    private void status4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status4MouseClicked
       changeStaus("4");
        setMemberName();
    }//GEN-LAST:event_status4MouseClicked

    private void status5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status5MouseClicked
       changeStaus("5");
        setMemberName();
    }//GEN-LAST:event_status5MouseClicked

    private void status6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status6MouseClicked
        changeStaus("6");
        setMemberName();
    }//GEN-LAST:event_status6MouseClicked

    private void status7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status7MouseClicked
       changeStaus("7");
        setMemberName();
    }//GEN-LAST:event_status7MouseClicked

    private void status8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status8MouseClicked
        changeStaus("8");
        setMemberName();
    }//GEN-LAST:event_status8MouseClicked

    private void status9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status9MouseClicked
        changeStaus("9");
        setMemberName();
    }//GEN-LAST:event_status9MouseClicked

    private void status10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status10MouseClicked
       changeStaus("10");
        setMemberName();
    }//GEN-LAST:event_status10MouseClicked

    private void status11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status11MouseClicked
        changeStaus("11");
        setMemberName();
    }//GEN-LAST:event_status11MouseClicked

    private void status12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_status12MouseClicked
        changeStaus("12");
        setMemberName();
    }//GEN-LAST:event_status12MouseClicked

    private void changeStaus(String id){
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "you want to change Status", "MEBP", dialogButton);
        if(dialogResult == 0) {
          System.out.println("Yes option");
          String memberValue =getValueByKey(id);
            if(getMemberStatus(memberValue).equals("1")){
                setValueByKey(id, getMemberName(memberValue)+","+0);
            }else{
                setValueByKey(id, getMemberName(memberValue)+","+1);
            }
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JLabel imgLoading;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblName10;
    private javax.swing.JLabel lblName11;
    private javax.swing.JLabel lblName12;
    private javax.swing.JLabel lblName2;
    private javax.swing.JLabel lblName3;
    private javax.swing.JLabel lblName4;
    private javax.swing.JLabel lblName5;
    private javax.swing.JLabel lblName6;
    private javax.swing.JLabel lblName7;
    private javax.swing.JLabel lblName8;
    private javax.swing.JLabel lblName9;
    private javax.swing.JLabel lblWinner;
    private javax.swing.JLabel lblWinnerlbl;
    private javax.swing.JPanel panal1;
    private javax.swing.JPanel pnl1;
    private javax.swing.JPanel pnl10;
    private javax.swing.JPanel pnl11;
    private javax.swing.JPanel pnl12;
    private javax.swing.JPanel pnl2;
    private javax.swing.JPanel pnl3;
    private javax.swing.JPanel pnl4;
    private javax.swing.JPanel pnl5;
    private javax.swing.JPanel pnl6;
    private javax.swing.JPanel pnl7;
    private javax.swing.JPanel pnl8;
    private javax.swing.JPanel pnl9;
    private javax.swing.JPanel pnlWinner;
    private javax.swing.JLabel status1;
    private javax.swing.JLabel status10;
    private javax.swing.JLabel status11;
    private javax.swing.JLabel status12;
    private javax.swing.JLabel status2;
    private javax.swing.JLabel status3;
    private javax.swing.JLabel status4;
    private javax.swing.JLabel status5;
    private javax.swing.JLabel status6;
    private javax.swing.JLabel status7;
    private javax.swing.JLabel status8;
    private javax.swing.JLabel status9;
    // End of variables declaration//GEN-END:variables

    
    private Timer timer = new Timer();
    
    public synchronized void abcCaller() {
        this.timer.cancel(); //this will cancel the current task. if there is no active task, nothing happens
        this.timer = new Timer();
            TimerTask action = new TimerTask() {
            public void run() {
                myfunction(); //as you said in the comments: abc is a static method
            }
        };
        this.timer.schedule(action, 10000); //this starts the task
    }
    
    
    private void myfunction() {
        ArrayList<Integer>  lista =getRamdomList();
        if(lista.size()<=0){
            imgLoading.setVisible(false);
            return;
        }
        Random r = new Random();
        int randomValue = lista.get(r.nextInt(lista.size()));
        //System.out.println(randomValue);
        String winner = getMemberName(getValueByKey(Integer.toString(randomValue)));
        //System.out.println("winner is : "+winner);
        pnlWinner.setVisible(true);
        lblWinner.setText(randomValue+" : "+winner);
        setValueByKey(Integer.toString(randomValue), winner+","+1);
        setMemberName();
        imgLoading.setVisible(false);
    }
    
    
    private ArrayList<Integer> getRamdomList(){
        ArrayList<Integer>  lista = new ArrayList<Integer>();
        Properties prop = new Properties();
	InputStream input = null;
	try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
           
            if(!getMemberStatus(prop.getProperty("1")).equals("1")){
                lista.add(1);
            }
            if(!getMemberStatus(prop.getProperty("2")).equals("1")){
                lista.add(2);
            }
            if(!getMemberStatus(prop.getProperty("3")).equals("1")){
                lista.add(3);
            }
            if(!getMemberStatus(prop.getProperty("4")).equals("1")){
                lista.add(4);
            }
            if(!getMemberStatus(prop.getProperty("5")).equals("1")){
                lista.add(5);
            }
            if(!getMemberStatus(prop.getProperty("6")).equals("1")){
                lista.add(6);
            }
            if(!getMemberStatus(prop.getProperty("7")).equals("1")){
                lista.add(7);
            }
            if(!getMemberStatus(prop.getProperty("8")).equals("1")){
                lista.add(8);
            }
            if(!getMemberStatus(prop.getProperty("9")).equals("1")){
                lista.add(9);
            }
            if(!getMemberStatus(prop.getProperty("10")).equals("1")){
                lista.add(10);
            }
            if(!getMemberStatus(prop.getProperty("11")).equals("1")){
                lista.add(11);
            }
            if(!getMemberStatus(prop.getProperty("12")).equals("1")){
                lista.add(12);
            }

	} catch (IOException ex) {
            ex.printStackTrace();
	} finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
        return lista;
    }
    
    private String getValueByKey(String key){
        String value = "";
        Properties prop = new Properties();
	InputStream input = null;
	try {
            input = new FileInputStream("config.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            //System.out.println("key is "+key);
            value = prop.getProperty(key);
            //System.out.println("value is "+value);

	} catch (IOException ex) {
            ex.printStackTrace();
	} finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
	}
        return value;
    }
    
    private void setValueByKey(String key,String value){
        Properties prop = new Properties();
        OutputStream output = null;
        FileInputStream in = null;
	try {
            in = new FileInputStream("config.properties");
            prop.load(in);
            output = new FileOutputStream("config.properties");
            // set the properties value
            prop.setProperty(key, value);
            prop.store(output, null);
	} catch (IOException io) {
            io.printStackTrace();
	} finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

	}
    }
    
    private String getMemberName(String value){
        String [] nameArrary = value.split(",");
        return nameArrary[0];
    }
    
    private String getMemberStatus(String value){
        String [] nameArrary = value.split(",");
        if(nameArrary.length >1){
            return nameArrary[1];
        }else{
            return "";
        }
    }
}

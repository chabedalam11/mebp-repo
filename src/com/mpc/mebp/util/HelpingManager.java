/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpc.mebp.util;

import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author medisys
 */
public class HelpingManager {

    public HelpingManager() {
    }
    
    public void setBackgroundImage1(JFrame jFrame){
        //set image on jframe background
        jFrame.setContentPane(new JLabel(new ImageIcon(getClass().getResource("/resource/bg1.png"))));
    }
    public void setBackgroundImage2(JFrame jFrame){
        //set image on jframe background
        jFrame.setContentPane(new JLabel(new ImageIcon(getClass().getResource("/resource/bg2.png"))));
    }
    
    public void setHeading(JFrame jFrame){
        //set jframe on center
        jFrame.setLocationRelativeTo(null);
        //set heading name and image
        jFrame.setTitle(" >>>>> MEBP <<<<<");
        jFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resource/medisysLogo.jpg")));
        
    }
    
}
